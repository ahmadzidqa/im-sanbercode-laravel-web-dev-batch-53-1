<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("Ape.php");

    echo "<h2> Latihan OOP </h2>";

    $sheep = new Animal("shaun");
        echo "Nama: " . $sheep->name . "<br>"; // "shaun"
        echo "Legs: " . $sheep->legs . "<br>"; // 4
        echo "Cold blooded: " . $sheep->cold_blooded . "<br> <br>"; // "no"
        
        $kodok = new Frog("buduk");
        echo "Nama: " . $kodok->name . "<br>"; 
        echo "Legs: " . $kodok->legs . "<br>"; 
        echo "Cold blooded: " . $kodok->cold_blooded . "<br>"; 
        echo "Jump: " . $kodok->jump() . "<br> <br>"; 

        $sungokong = new Ape("kera sakti");
        echo "Nama: " . $sungokong->name . "<br>"; 
        echo "Legs: " . $sungokong->legs . "<br>"; 
        echo "Cold blooded: " . $sungokong->cold_blooded . "<br>"; 
        echo "Yell: " . $sungokong->yell() . "<br> <br>"; 

        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>
