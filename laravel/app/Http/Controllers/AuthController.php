<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function datang(){
        return view("pages.form");

    }

    public function daftar(){
        return view("register");

    }

    public function selamat(Request $request) {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
      
        return view( 'pages.form', ['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}