<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/form" method="post">
        @csrf
        <h2>Sign Up Form</h2>
        <label>First Name:</label> <br> <br>
        <input type="text" name="fname" class="fname"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text" name="lname" class="lname"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender">Male <br> 
        <input type="radio" name="Gender">Female <br> 
        <input type="radio" name="Gender">Other <br> <br>
        <label>Nationalty:</label> <br> <br>
        <select>
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Inggris</option>
            <option value="">Belanda</option>
        </select> <br> <br>
        <label>Languange Spoken:</label> <br> <br>
        <input type="checkbox" name="Languange Spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="Languange Spoken">Bahasa Inggris <br> 
        <input type="checkbox" name="Languange Spoken">Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign up" >
    </form>
</body>
</html>